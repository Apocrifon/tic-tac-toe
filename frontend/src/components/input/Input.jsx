import React from 'react';
import { observer } from 'mobx-react-lite';

const Input = observer(({
  className, placeholder, change, type, value
}) => (
  <input
    className={`${className}`}
    placeholder={placeholder}
    type={type}
    onChange={(event) => { change(event) }}
    value={value}
  />
));

export default Input;
