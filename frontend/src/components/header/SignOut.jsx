import React from 'react';
import { Link } from 'react-router-dom';

function SignOut({ text, path, className}) {
  const clearData = () => {
    document.cookie.split(";").forEach((c) => {
      document.cookie = c
        .replace(/^ +/, "")
        .replace(/=.*/, `=;expires=${new Date().toUTCString()};path=/`);
		});
    localStorage.removeItem('user');
    localStorage.removeItem('messages');
    localStorage.removeItem('gameData');
    localStorage.removeItem('players');
		window.location.reload();
  };

  return (
    <Link to={path}>
      <button className={`${className}`} onClick={clearData}>
        {text}
      </button>
    </Link>
  );
} 

export default SignOut;