import React from 'react';
import { Link } from 'react-router-dom';

function PublicRoute({ text, path, className }) {
  return (
    <Link to={path}>
      <button className={`${className}`}>
        {text}
      </button>
    </Link>
  );
}

export default PublicRoute;
