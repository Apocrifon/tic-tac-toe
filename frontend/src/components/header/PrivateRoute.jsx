import React, { useEffect, useState } from 'react';
import { Link, Navigate } from 'react-router-dom';
import axios from 'axios';

const config = {
  headers: {
    Authorization: `Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiIyMjgiLCJuYW1lIjoiU3BlcGFuIn0.rR13rE9kZhv88tSFb4F1EQ-5j_i7hFDf1G0utxHHWE4`,
  },
};

function PrivateRoute({ text, path, className}) {

  const [isAuth, setIsAuth] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    checkAuthentication();
  }, []);

  const checkAuthentication = async () => {
    axios.post('/api/token', {}, config)
      .then(res => {
        setIsAuth(res.data);
        console.log(res.data);
      })
      .catch(err => console.log(err));
    setIsLoading(false);
  };

  if (isLoading) {
    return <div>Loading...</div>;
  }

  if (isAuth) 
    return (
      <Link to={path}>
        <button className={`${className}`}>
          {text}
        </button>
      </Link>
    )
  
  return (<Navigate to="/" />)
  
}

export default PrivateRoute;
