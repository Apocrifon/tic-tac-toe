import React from 'react';
import styles from './styles.module.css';
import PublicRoute from './PublicRoute';
import SignOut from './SignOut';


 
function Header({ activePage }) {

  return (
    <header>
      <img src="static/images/xo-logo.svg" alt="error" />
      <nav className={styles}>
        <ol>
          <PublicRoute className={activePage === 'Игровое поле' ? styles.activeButton : styles.defaultButton}
            text="Игровое поле" path="/battle" />
          <PublicRoute className={activePage === 'Рейтинг' ? styles.activeButton : styles.defaultButton}
            text="Рейтинг" path="/rating" />
          <PublicRoute className={activePage === 'Активные игроки' ? styles.activeButton : styles.defaultButton}
            text="Активные игроки" path="/active"  />
          <PublicRoute className={activePage === 'История игр' ? styles.activeButton : styles.defaultButton}
            text="История игр" path="/history"  />
          <PublicRoute className={activePage === 'Список игроков' ? styles.activeButton : styles.defaultButton}
            text="Список игроков" path="/players"/>
        </ol>
      </nav>
      <SignOut className={styles.exit} path="/"/>
    </header>
  );
}

export default Header;
