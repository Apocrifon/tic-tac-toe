import React from 'react';

function H1(props) {
  return (
    <h1 style={
      {
        fontWeight: '700',
        fontSize: '24px',
        lineHeight: '150%',
        color: '#373745',
        margin: '0px',
      }
    }
    >
      {props.text}
    </h1>
  );
}
export default H1;
