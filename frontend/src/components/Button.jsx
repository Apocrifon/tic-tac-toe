import React from 'react';

function Button({
  className, click, image, text }) {
  
  return (
    <button className={className} onClick={() => click()}>
      {image != null && <img src={image} alt="error" />}
      {text}
    </button>
  );
}

export default Button;
