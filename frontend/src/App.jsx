import React, { useEffect, useState } from 'react';
import { BrowserRouter, Routes, Route} from 'react-router-dom';
import axios from 'axios';
import Auth from './pages/auth/Auth';
import Battle from './pages/battle/Battle';
import Rating from './pages/rating/Rating';
import Active from './pages/active/Active';
import History from './pages/history/History';
import Players from './pages/players/Players';
import ProtectedRoute from './components/ProtectedRoute';


export const config = {
  headers: {
    Authorization: `Bearer ${document.cookie.split('=')[1]}`,
  },
};

function App() {
  const [isAuth, setIsAuth] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    checkAuthentication();
  });

  const checkAuthentication = async () => {
    try {
      const res = await axios.post('/api/token', {}, config);
      setIsAuth(res.data.isAuth);
      setIsLoading(false);
    } catch (err) {
      setIsLoading(false);
    }
  };
 
  if (isLoading) {
    return (<></>);
  }
  
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Auth auth={isAuth} />} />
        <Route
          path="/battle"
          element={
            <ProtectedRoute auth={isAuth}>
              <Battle/>
            </ProtectedRoute>
          }
        />
                <Route
          path="/rating"
          element={
            <ProtectedRoute auth={isAuth}>
              <Rating/>
            </ProtectedRoute>
          }
        />
                <Route
          path="/active"
          element={
            <ProtectedRoute auth={isAuth}>
              <Active/>
            </ProtectedRoute>
          }
        />
                <Route
          path="/history"
          element={
            <ProtectedRoute auth={isAuth}>
              <History/>
            </ProtectedRoute>
          }
        />
                <Route
          path="/players"
          element={
            <ProtectedRoute auth={isAuth}>
              <Players/>
            </ProtectedRoute>
          }
        />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
