import React from 'react';
import { action, observable } from 'mobx';
import { Navigate } from 'react-router-dom';
import axios from 'axios';
import { observer } from 'mobx-react-lite';
import styles from './styles.module.css';
import Input from '../../components/input/Input';
import Button from '../../components/Button';
import H1 from '../../components/H1';
import { config } from '../../App';

const UserData = observable({
  login: '',
  password: '',
  isAuthorized: false,
  tried: false,
});

const changeLogin = action((event) => {
  UserData.login = event.target.value;
});

const changePassword = action((event) => {
  UserData.password = event.target.value;
});

const Auth = observer(({ auth }) => {
  
  if (auth) {
    console.log(auth);
    return <Navigate to="/rating" />;
  }

  const checkPassword = async () => {
    try {
      const res = await axios.post('/auth', UserData, config);
      console.log(res.data.token);
      document.cookie = "jwt=" + res.data.token;
      localStorage.setItem('user', res.data.user);
      localStorage.setItem('mode', res.data.user===1 ? 'x' : 'o'); //
      console.log('fix');
      UserData.tried = false;
      window.location.replace('/battle');
    } catch (error) {
      console.error(error);
      UserData.tried = true; 
    }
  }
  

  const boaderError = UserData.tried === true ? 'boardError' : '';

  return (
    <div className={styles.auth}>
      <div className={styles.authСontainer}>
        <img src="static/images/dog.svg" alt="error" />
        <H1 text="Войдите в игру" />
        <form className={styles.inputForm} onSubmit={(event) => event.preventDefault()}>
          <div style={{ display: 'flex', flexDirection: 'column', gap: '12px' }}>
            <Input
              placeholder="Логин"
              className={`${styles.textInput} ${styles[boaderError]}`}
              change={changeLogin}
            />
            <div>
              <Input
                type="password"
                placeholder="Пароль"
                className={`${styles.textInput} ${styles[boaderError]}`}
                change={changePassword}
              />
              {UserData.tried && <div style={{ color: '#E93E3E', letterSpacing: '0.25px' }}>Неверный пароль</div>}
            </div>
          </div>
          <Button
            text="Войти"
            className={styles.authButton}
            click={checkPassword}
          />
        </form>
      </div>
    </div>
  );
});

export default Auth;
