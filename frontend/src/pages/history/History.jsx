import React from 'react';
import styles from './styles.module.css';
import Header from '../../components/header/Header';
import Table from './components/Table';

const games = [
  {
    id: 1,
    o: 'Терещенко У. Р.',
    x: 'Многогрешный П. В.',
    winner: 'o',
    date: '12 февраля 2022',
    gameTime: '43 мин 13 сек',
  },
  {
    id: 2,
    o: 'Терещенко У. Р.',
    x: 'Многогрешный П. В.',
    winner: 'o',
    date: '12 февраля 2022',
    gameTime: '43 мин 13 сек',
  },
  {
    id: 3,
    o: 'Терещенко У. Р.',
    x: 'Многогрешный П. В.',
    winner: 'x',
    date: '12 февраля 2022',
    gameTime: '43 мин 13 сек',
  },
  {
    id: 4,
    o: 'Терещенко У. Р.',
    x: 'Многогрешный П. В.',
    winner: 'o',
    date: '12 февраля 2022',
    gameTime: '43 мин 13 сек',
  },
];

function History() {
  return (
    <>
      <Header activePage="История игр" />
      <div className={styles.tableLocation}>
        <Table tableName="История игр" games={games} />
      </div>
    </>
  );
}

export default History;
