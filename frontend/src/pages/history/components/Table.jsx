import React from 'react';
import H1 from '../../../components/H1';
import Row from './Row';
import styles from './styles.module.css';

function Table(props) {
  return (
    <div className={styles.tableForm}>
      <H1 text={props.tableName} />
      <table className={styles.table}>
        <tbody>
          <tr className={styles.header}>
            <th>Игроки</th>
            <th>Дата</th>
            <th>Время игры</th>
          </tr>
          {props.games.map((game) => <Row key={game.id} game={game} />)}
        </tbody>
      </table>
    </div>
  );
}

export default Table;
