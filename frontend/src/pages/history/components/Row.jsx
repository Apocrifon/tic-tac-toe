import React from 'react';
import styles from './styles.module.css';

function Row(props) {
  return (
    <tr className={styles.row}>
      <td>
        <div className={styles.players}>
          <div className={styles.player}>
            <img src="static/images/small-zero.svg" alt="error" />
            {props.game.o}
            {props.game.winner === 'o' && <img style={{ height: '24px', width: '24px' }} src="static/images/small-cup.svg" alt="error" />}
          </div>
          <div className={styles.versus}>против</div>
          <div className={styles.player}>
            <img src="static/images/small-x.svg" alt="error" />
            {props.game.x}
            {props.game.winner === 'x' && <img style={{ height: '24px', width: '24px' }} src="static/images/small-cup.svg" alt="error" />}
          </div>
        </div>
      </td>
      <td>{props.game.date}</td>
      <td>{props.game.gameTime}</td>
    </tr>
  );
}

export default Row;
