import { makeAutoObservable } from 'mobx';
import gameLogic from './gameLogic';
import io from 'socket.io-client';

const emptyChat = [];

if (localStorage.getItem('messages') === null)
	localStorage.setItem('messages', JSON.stringify(emptyChat));
	
class Messages {

	constructor() {
		this.messages = JSON.parse(localStorage.getItem('messages'))
		this.currentText = '';
		this.socket = io.connect('http://localhost:3000');
		makeAutoObservable(this);
	}

	sendMessages(messages) {
		this.socket.emit('sendMessage', messages);
	}

	updateMessages(messages) {
		this.messages = messages;
	}
	
	sendMessage() {

		const player = gameLogic.playersMode === 'x' ? 'xPlayer' : 'oPlayer';

		const newMessage = {
				id: this.messages.length=== 0 ? 1 : this.messages.at(-1).id+1,
				text: this.currentText,
				time: new Date().toLocaleTimeString('en-US', {
					hour12: false,
					hour: 'numeric',
					minute: 'numeric',
				}),
			player: JSON.parse(localStorage.getItem('players'))[player],
			type : gameLogic.mode,
		}
		this.messages = [...this.messages, newMessage];
		this.sendMessages(this.messages);
		localStorage.setItem('messages', JSON.stringify(this.messages));
		this.currentText = '';
	}
}

export default new Messages();