import { makeAutoObservable } from 'mobx';

class PlayerStore {

	constructor() {
		this.xPlayer = {
			id : 1,
			name: 'Плюшкина Екатерина Викторовна',
			shortName: 'Плюшкина Екатерина',
			winRate: '23',
			type: 'x',
			smallImage: 'static/images/x.svg',
			bigImage: 'static/images/xxl-x.svg',
		};
		this.oPlayer = {
			id : 2,
			name: 'Пупкин Владелен Игоревич',
			shortName: 'Пупкин Владелен',
			winRate: '63',
			type: 'o',
			smallImage: 'static/images/zero.svg',
			bigImage: 'static/images/xxl-zero.svg',
		};
		makeAutoObservable(this);
	}

	getPlayersInfo() {
		const data = {xPlayer:this.xPlayer, oPlayer: this.oPlayer };
		return data;
	}
}

export default new PlayerStore();