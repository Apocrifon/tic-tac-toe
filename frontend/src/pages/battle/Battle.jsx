import React, { useState, useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import styles from './styles.module.css';
import Header from '../../components/header/Header';
import PlayersTable from './components/PlayerTable';
import Winner from './components/Winner';
import Game from './gameLogic';
import playerStore from './playerStore';

const Battle = observer(() => {

  const [players, setPlayers] = useState(null);

  useEffect(() => {
    let res = playerStore.getPlayersInfo();
      setPlayers(res);
      localStorage.setItem('players', JSON.stringify(res));
  }, []);

  return (players &&
    <>
      <Header activePage="Игровое поле" />
      <div className={styles.battle}>
      <PlayersTable players={players} />
      </div>
      {Game.isGameOver && <Winner players={players}/>}
    </>
  )
});

export default Battle;
