import { makeAutoObservable } from 'mobx';
import playerStore from './playerStore';
import io from 'socket.io-client';

const startPosition ={
  mode: 'x',
  field: [
    [null, null, null],
    [null, null, null],
    [null, null, null],
  ],
  time: 0,
  isGameStart: false,
}
if (localStorage.getItem('gameData') === null)
  localStorage.setItem('gameData', JSON.stringify(startPosition));

class Game {
 
  data = JSON.parse(localStorage.getItem('gameData'));

  constructor() {
    this.data = JSON.parse(localStorage.getItem('gameData'));
    this.mode = this.data.mode;
    this.playersMode = localStorage.getItem('mode');  ///
    console.log('fix');
    this.field = this.data.field;
    this.time = this.data.time;
    this.isGameStart = this.data.isGameStart;
    this.players = null;
    if (this.isGameStart)
      this.updateTime();
    this.players = playerStore.getPlayersInfo();
    this.socket = io.connect('http://localhost:3000');
    makeAutoObservable(this);
  } 

  sendMove(field) {
		this.socket.emit('sendField', field);
	}

  updateField(cell) {
    const row = (cell) / 3;
    const column = (cell) % 3;
    if (this.field[row][column] == null) {
      if (!this.isGameStart)
        this.updateTime();
      this.field[row][column] = this.mode;
      this.data.field = this.field;
      this.isGameStart = true;
      this.data.isGameStart = true;
      if (!(this.isGameOver)) {
        this.setMode();
      }
    }
    localStorage.setItem('gameData', JSON.stringify(this.data));
	}

  updateTime() {
    const id = setInterval(() => {
      if (this.isGameOver) {
        clearInterval(id);
        return;
      }
      this.addSecond()
      this.data.time++;
      localStorage.setItem('gameData', JSON.stringify(this.data));
    }, 1000);
  }

  addSecond() {
    this.time += 1;
  }

  get getTime() {
    return this.time;
  }

  get getMode() {
    return this.mode;
  }

  get haveWinner() {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
      const [x, y, z] = lines[i];

      const line = this.field.flat(1);
      if (line[x] && line[x] === line[y]
        && line[y] === line[z]) {
        return true;
      }
    }
    return false;
  }

  get haveFreeCells() {
    for (let i = 0; i < 3; i++) {
      for (let j = 0; j < 3; j++) {
        if (this.field[i][j] == null) return true;
      }
    }
    return false;
  }

  get isGameOver() {
    if (this.haveWinner || !this.haveFreeCells) {
      return true;
    }
    return false;
  }

  get gameResult() {
    if (this.haveWinner) return this.mode;
    return null;
  }

  get winnerCells() {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
      const [x, y, z] = lines[i];

      const line = this.field.flat(1);
      if (line[x] && line[x] === line[y]
        && line[y] === line[z]) {
        return lines[i];
      }
    }
    return [];
  }

  setMode() {
    this.data.mode = this.mode === 'o' ? 'x' : 'o';
    this.mode = this.mode === 'o' ? 'x' : 'o';
  }

  state(cell) {
    const row = (cell) / 3;
    const column = (cell) % 3;
    return this.field[row][column];
  }

  get gameFieldStatus() {
    return this.field;
  }

  currentPlayer() {
    let res;
    if (this.mode === 'x')
      playerStore.getPlayersInfo().then(player => res = this.player.xPlayer);
    else
      playerStore.getPlayersInfo().then(player => res = this.player.oPlayer);
    return res;
  }

  makeMove(cell) {
    if (this.mode!== this.playersMode)
      return;
    const row = (cell) / 3;
    const column = (cell) % 3;
    if (this.field[row][column] == null) {
      if (!this.isGameStart)
        this.updateTime();
      this.field[row][column] = this.playersMode;
      this.data.field = this.field;
      this.isGameStart = true;
      this.data.isGameStart = true;
      if (!(this.isGameOver)) {
        this.setMode();
      }
    }
    else {
      console.log('Клетка занята');
    }
    localStorage.setItem('gameData', JSON.stringify(this.data));
    this.sendMove(cell);
  }

  startNewGame() {
    this.mode = 'x';
    this.field = [
      [null, null, null],
      [null, null, null],
      [null, null, null],
    ];
    this.time = 0;
    this.isGameStart = false;
    localStorage.setItem('gameData', JSON.stringify(startPosition));
  }
}

export default new Game();
