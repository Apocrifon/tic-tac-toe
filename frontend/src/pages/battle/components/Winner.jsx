import React from 'react';
import { observer } from 'mobx-react-lite';
import Button from '../../../components/Button';
import H1 from '../../../components/H1';
import styles from './styles.module.css';
import PublicRoute from '../../../components/header/PublicRoute';
import Game from '../gameLogic'

const Winner = observer(({ players }) => {
  let winner;
  if (Game.gameResult === null)
    winner = 'Ничья';
  if (Game.gameResult === 'x')
    winner = players.xPlayer.shortName + ' победил';
  if (Game.gameResult === 'o')
    winner = players.oPlayer.shortName + ' победил';

  return (Game.currentPlayer &&
    <div className={styles.modalPosition}>
      <div className={styles.modal}>
        <img src="static/images/big-cup.svg" alt="error" />
        <H1 text={winner} />
        <div style={{ display: 'flex', flexDirection: 'column', gap: '12px' }}>
          <Button text="Новая игра" className={styles.newGameButton} click={() =>Game.startNewGame()} />
          <PublicRoute text="Выйти в меню" className={styles.quitToMenuButton} path="/rating" />
        </div>
      </div>
    </div>
  );
});

export default Winner;
