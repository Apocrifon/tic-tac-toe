import React from 'react';
import { observer } from 'mobx-react-lite';
import H1 from '../../../components/H1';
import Chat from './Chat';
import Game from './Game';
import PlayerInfo from './PlayersInfo';
import styles from './styles.module.css';

const PlayersTable = observer(({players}) => (
  <>
    <div className={styles.playerTable}>
      <H1 text="Игроки" />
      <PlayerInfo side="o" players={players} />
      <PlayerInfo side="x" players={players}/>
    </div>
    <Game players={players} />
    <Chat/>
  </>
));

export default PlayersTable;
