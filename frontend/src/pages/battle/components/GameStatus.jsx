import React from 'react';
import styles from './styles.module.css';
import { observer } from 'mobx-react-lite';
import Game from '../gameLogic';

const GameStatus = observer(({ players }) => {

  const player = Game.mode === 'x' ? players.xPlayer : players.oPlayer;

  return (player &&
    <div className={styles.turn}>
      Ходит
      <img src={player.smallImage} alt="error" />
      {player.shortName}
    </div>
  );
})

export default GameStatus;
