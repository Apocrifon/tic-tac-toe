import React from 'react';
import { observer } from 'mobx-react-lite';
import H1 from '../../../components/H1';
import styles from './styles.module.css';
import Game from '../gameLogic'

const Timer = observer(() => {
  const minutes = Math.floor(Game.getTime / 60);
  const remainingSeconds = Game.getTime % 60;

  return (
    <div className={styles.time}>
      <H1 text={`${minutes}:${remainingSeconds < 10 ? '0' : ''}${remainingSeconds}`} />
    </div>
  );
});

export default Timer;
