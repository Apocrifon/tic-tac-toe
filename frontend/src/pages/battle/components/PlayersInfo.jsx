import React from 'react';
import styles from './styles.module.css';
import { observer } from 'mobx-react-lite';

const PlayerInfo = observer(({ side, players }) => {
  
  const player = side === 'x' ? players.xPlayer : players.oPlayer;

  return (player &&
    <div className={styles.infoBlock}>
      <div><img style={{ height: '24px' }} src={player.smallImage} alt="error" /></div>
      <div className={styles.userInfo}>
        <div className={styles.name}>{player.name}</div>
        <div className={styles.winrate}>
          {player.winRate}
          {' '}
          % побед
        </div>
      </div>
    </div>
  );
})

export default PlayerInfo;
