import React from 'react';
import Button from '../../../components/Button';
import styles from './styles.module.css';

function Cell({value, click, winCell, winner,}) {
  let img;
  if (value != null) {
    img = value === 'x' ? 'static/images/xxl-x.svg' : 'static/images/xxl-zero.svg';
  }
  let win = '';
  if (winCell) win = winner === 'o' ? 'oWinnerCell' : 'xWinnerCell';

  return (
    <Button click={click} className={`${styles.cellButton} ${styles[win]}`} image={img} />
  );
}

export default Cell;
