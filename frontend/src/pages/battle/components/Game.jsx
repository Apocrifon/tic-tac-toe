import React from 'react';
import { observer } from 'mobx-react-lite';
import styles from './styles.module.css';
import Timer from './Timer';
import Field from './Field';
import GameStatus from './GameStatus';

export const Game = observer(({players}) => {

  return (
    <div className={styles.game}>
      <div className={styles.gameZone}>
        <Timer/>
        <Field/>
        <GameStatus players={players} />
      </div>
    </div>
  );
});

export default Game;
