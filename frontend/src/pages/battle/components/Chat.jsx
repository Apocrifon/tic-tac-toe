import React, {useEffect} from 'react';
import { action } from 'mobx';
import { observer } from 'mobx-react-lite';
import Input from '../../../components/input/Input';
import Message from './Message';
import styles from './styles.module.css';
import Button from '../../../components/Button';
import messageStore from '../messageStore';

const Chat = observer(() => {
  
  const changeMessage = action((event) => {
    messageStore.currentText = event.target.value;
  });

  const sendMessage = action(() => {
    messageStore.sendMessage();
  });

  useEffect(() => {
    messageStore.socket.on('recieveMessage', messages => {
      messageStore.updateMessages(messages)
    })
  }, []);

  return (
    <div className={styles.chat}>
      <div className={styles.fixChat}>
        <div className={styles.messages}>
          {messageStore.messages.map((message) => (
            <Message
              player={message.player}
              time={message.time}
              text={message.text}
              key={message.id}
            />
          ))}
        </div>
      </div>
      <div className={styles.messageForm}>
        <Input
          placeholder="Сообщение…"
          className={styles.textInput}
          text={messageStore.currentText}
          change={changeMessage}
          value={messageStore.currentText}
        />
        <Button className={styles.sendButton} click={sendMessage} />
      </div>
    </div>
  );
});

export default Chat;
