import React from 'react';
import classnames from 'classnames';
import styles from './styles.module.css';

function Message({ player, time, text }) {
  
  const colorStyle = player.type.toString();
  return (
    <div className={classnames(styles.message, styles[colorStyle])}>
      <div className={styles.nameTimeBlock}>
        <div className={classnames(styles.messName, styles[colorStyle])}>{player.shortName}</div>
        <div className={styles.sendTime}>{time}</div>
      </div>
      <div className={styles.text}>{text}</div>
    </div>
  );
}

export default Message;
