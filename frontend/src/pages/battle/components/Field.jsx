import { observer } from 'mobx-react-lite';
import React, {useEffect} from 'react';
import Cell from './Cell';
import styles from './styles.module.css';
import Game from '../gameLogic';

const Field = observer(() => {
  const indexes = [...Array(9).keys()];
  const { winnerCells } = Game;

  function Move(index) {
      Game.makeMove(index);   
  }

  useEffect(() => {
    Game.socket.on('recieveField', field => {
      Game.updateField(field);
    })
  }, []);

  return (
    <div className={styles.field}>
      {indexes.map((index) => (
        <Cell
          key={index}
          value={Game.state(index)}
          winCell={winnerCells.includes(index)}
          click={() => Move(index)}
          winner={Game.gameResult}
        />
      ))}
    </div>
  );
});

export default Field;
