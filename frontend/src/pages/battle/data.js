export const player1 = {
  name: 'Пупкин Владелен Игоревич',
  winrate: '63',
};

export const player2 = {
  name: 'Плюшкина Екатерина Викторовна',
  winrate: '23',
};

export const xPlayer = {
  name: 'Плюшкина Екатерина',
  patronymic: 'Викторовна',
  winRate: '23% побед',
  type: 'x',
  smallImage: 'static/images/x.svg',
  bigImage: 'static/images/xxl-x.svg',
};

export const oPlayer = {
  name: 'Пупкин Владелен',
  patronymic: 'Игоревич',
  winRate: '63% побед',
  type: 'o',
  smallImage: 'static/images/zero.svg',
  bigImage: 'static/images/xxl-zero.svg',
};