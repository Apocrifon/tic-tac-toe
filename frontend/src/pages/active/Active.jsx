import React from 'react';
import styles from './styles.module.css';
import Header from '../../components/header/Header';
import { players } from './data';
import Table from './components/Table';

function Active() {
  return (
    <>
      <Header activePage="Активные игроки" />
      <div className={styles.tableLocation}>
        <Table players={players} />
      </div>
    </>
  );
}

export default Active;
