import React, { useState } from 'react';
import styles from './styles.module.css';
import H1 from '../../../components/H1';
import Switch from './Switch';
import Row from './Row';

function Table({ players }) {
  const [onlyFree, setOnlyFree] = useState('');

  return (
    <div className={styles.tableForm}>
      <div className={styles.header}>
        <H1 text="Активные игроки" />
        <div style={{ display: 'flex', gap: '8px' }}>
          <span>Только свободные</span>
          <Switch value={onlyFree} onChange={setOnlyFree} />
        </div>
      </div>
      <table className={styles.table}>
        <tbody>
          {players.map((player) => ((!onlyFree || player.status === 'Свободен') && <Row key={player.id} player={player} />
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default Table;
