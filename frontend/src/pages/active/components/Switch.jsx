import React from 'react';
import styles from './styles.module.css';

function Switch({ value, onChange }) {
  return (
    <div style={{ alignItems: 'center', display: 'flex' }}>
      <label className={styles.onlyFree}>
        <input type="checkbox" onChange={() => onChange(value === '' ? 'Свободен' : '')} />
        <div className={styles.ball} />
      </label>
    </div>
  );
}

export default Switch;
