import React, {useEffect, useState} from 'react';
import { Navigate } from 'react-router-dom';
import { observer } from 'mobx-react-lite';
import Button from '../../../components/Button';
import styles from './styles.module.css';
import { invite, socket,players } from '../data';

const Row = observer(({ player }) => {

  const [inGame, setInGame] = useState(false);

  const updateStatus = (id) => {
    for (let i = 0; i < players.length; i++) {
      if (players[i].id === id)
        players[i].status = 'В игре';
    }
  }
  
    useEffect(() => {
      socket.on('recieveInvite', invite => {
        updateStatus(invite);
        if (+localStorage.getItem('user') === player.id && player.status==='В игре')
          setInGame(true);
      })
    });
  
  if (inGame)
    return <Navigate to='/battle'/>
    
    
  const statusStyle = player.status === 'Свободен' ? 'free' : 'inGame';
  return (
    <tr key={player.id}>
      <td>{player.name}</td>
      <td>
        <div className={styles[statusStyle]}>{player.status}</div>
        <Button className={styles[`${statusStyle}Button`]} text="Позвать играть" click={() => invite(player)} />
      </td>
    </tr>
  );
});

export default Row;
