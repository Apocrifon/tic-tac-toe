import { action, observable } from 'mobx';
import io from 'socket.io-client';

export const socket = io.connect('http://localhost:3000');



export const players = observable([
  {
    id: 1,
    name: 'Александров Игнат Анатолиевич',
    status: 'Свободен',
  },
  {
    id: 2,
    name: 'Александров Игнат Анатолиевич',
    status: 'В игре',
  },
  {
    id: 3,
    name: 'Александров Игнат Анатолиевич',
    status: 'Свободен',
  },
  {
    id: 4,
    name: 'Александров Игнат Анатолиевич',
    status: 'В игре',
  },
  {
    id: 5,
    name: 'Александров Игнат Анатолиевич',
    status: 'В игре',
  },
  {
    id: 6,
    name: 'Александров Игнат Анатолиевич',
    status: 'Свободен',
  },
]);

export const invite = action((user) => {
  user.status = 'В игре';
  socket.emit('sendInvite', user.id);
});




