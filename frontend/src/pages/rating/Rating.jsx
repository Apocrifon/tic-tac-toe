import React, { useEffect, useState } from 'react';
import axios from 'axios';
import styles from './styles.module.css';
import Header from '../../components/header/Header';
import Table from './components/Table';
import { config } from '../../App';

const rowName = ['ФИО', 'Всего игр', 'Победы', 'Проигрыши', 'Процент побед'];

function Rating() {

  const [data, setData] = useState(null);

  useEffect(() => {
    axios.get('/rating', config)
      .then(res => {
        setData(res.data);
      })
    .catch(err => console.log(err))
  }, []);


  return (
    <>
      <Header activePage="Рейтинг" />
      <div className={styles.tableLocation}>
        {data && (<Table rowNames={rowName} data={data} styles={styles} tableName="Рейтинг игроков" />)}
      </div>
    </>
  );
}

export default Rating;
