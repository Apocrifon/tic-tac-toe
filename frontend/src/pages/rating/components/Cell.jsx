import React from 'react';

function Cell(props) {
  return <td>{props.value}</td>;
}

export default Cell;
