import React from 'react';
import Cell from './Cell';

function Row({ data }) {

  const user = {
    name:  [data.surname,data.firstname,data.secondname].join(' '),
    gamesCount: data.games_cnt,
    gamesWins: data.games_wins,
    gamesLoses: data.games_loses,
    persent: Math.round(data.percent*100)+'%',
  };

  return (
    <tr>
      {Object.entries(user).map(([key, value]) => (
      <Cell key={key} value={value} />
    ))}
    </tr>
  );
}

export default Row;
