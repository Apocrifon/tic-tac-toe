import React from 'react';
import H1 from '../../../components/H1';
import Row from './Row';
import styles from './styles.module.css';

export function Table({ data, rowNames, tableName }) {
  let rowNamesKey = 0;
  let rowCellsKey = 0;
  return (
    <div className={styles.tableForm}>
      <H1 text={tableName} />
      <table className={styles.table}>
        <tbody>
          <tr>
            {rowNames.map((row) => (
              <th key={rowNamesKey++}>{row}</th>
            ))}
          </tr>

          {data.map((row) => <Row key={rowCellsKey++} data={row} />)}
        </tbody>
      </table>
    </div>
  );
}

export default Table;
