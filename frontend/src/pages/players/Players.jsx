import React, { useState } from 'react';
import { action, observable } from 'mobx';
import styles from './styles.module.css';
import Header from '../../components/header/Header';
import Table from './components/Table';
import AddPlayer from './components/AddPlayer';

export var NowId = 4;

const players = observable([
  {
    id: 1,
    name: 'Александров Игнат Анатолиевич',
    age: '24',
    gender: 'man',
    status: 'Заблокирован',
    createDate: '12 октября 2021',
    changedDate: '22 октября 2021',
  },
  {
    id: 2,
    name: 'Мартынов Остап Фёдорович',
    age: '24',
    gender: 'woman',
    status: 'Активен',
    createDate: '12 октября 2021',
    changedDate: '22 октября 2021',
  },
  {
    id: 3,
    name: 'Александров Игнат Анатолиевич',
    age: '24',
    gender: 'man',
    status: 'Заблокирован',
    createDate: '12 октября 2021',
    changedDate: '22 октября 2021',
  },
]);

export const addPlayer = action((player) => {
  players.push(player);
  NowId++;
});

export const changeStatus = action((player) => {
  player.status = player.status === 'Заблокирован' ? 'Активен' : 'Заблокирован';
});

function Players() {
  const [visible, setVisible] = useState(false);
  return (
    <>
      <Header activePage="Список игроков" />
      <div className={styles.tableLocation}>
        <Table tableName="Список игроков" players={players} show={() => setVisible(true)} />
      </div>
      {visible && <AddPlayer add={addPlayer} show={() => setVisible(false)} />}
    </>
  );
}

export default Players;
