import Button from "../../../components/Button";
import H1 from "../../../components/H1";
import Row from "./Row";
import styles from "./styles.module.css"

const Table = ({tableName, players, show}) => (
	<div className={styles.tableForm}>
		<div style={{ display: "flex", justifyContent: "space-between", alignItems: "center"}}>
			<H1 text={tableName} />
			<Button text={"Добавить игрока"} className={styles.addPlayerButton} click={show} />
		</div>
		<table className={styles.table}>
			<tbody>
				<tr className={styles.header}>
					<th>ФИО</th>	
					<th>Возраст</th>
					<th>Пол</th>
					<th>Статус</th>	
					<th>Создан</th>
					<th>Изменен</th>
					<th></th>
				</tr>
				{players.map(player => 
					<Row key={player.id} user={player}/>
					)}
			</tbody>
		</table>
	</div>
)

export default Table;