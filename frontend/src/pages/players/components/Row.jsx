import { observer } from "mobx-react-lite";
import Button from "../../../components/Button";
import styles from "./styles.module.css"
import { changeStatus } from '../Players'

const Row = observer(({user}) => {
	const genderImg = user.gender === 'men' ? "/static/images/men.svg" : "/static/images/women.svg";
	const btnText = user.status === "Заблокирован" ? "Разблокировать" : "Заблокировать";
	const btnImg = user.status === "Заблокирован" ? null : "/static/images/block.svg";
	const btnStyle = user.status === "Заблокирован" ? "unlock" : "lock";
	const labelStatus = user.status === "Заблокирован" ? "blocked" : "active";

	return (
		<tr className={styles.row}>
			<td>{user.name}</td>
			<td>{user.age }</td>
			<td><img src={genderImg} alt='error'/></td>
			<td><div className={styles[labelStatus]}>{user.status}</div></td>
			<td>{user.createDate }</td>
			<td>{user.changedDate}</td>
			<td><Button text={btnText} image={btnImg} className={styles[btnStyle]} click={() => changeStatus(user)}/></td>
		</tr>
	)
})


export default Row;