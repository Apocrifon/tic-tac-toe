import React from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react-lite';
import Button from '../../../components/Button';
import H1 from '../../../components/H1';
import styles from './styles.module.css';
import Input from '../../../components/input/Input';
import { addPlayer, NowId } from '../Players';

const date = new Date();
const options = { day: 'numeric', month: 'long', year: 'numeric' };
const formatter = new Intl.DateTimeFormat('ru-RU', options);
const formattedDate = formatter.format(date);

const newPlayer = observable({
  id: NowId,
  name: '',
  age: '',
  gender: 'women',
  status: 'Активен',
  createDate: formattedDate,
  changedDate: formattedDate,
});

const changeGender = action((gender) => {
  newPlayer.gender = newPlayer.gender !== gender ? gender : newPlayer.gender;
});

const AddPlayer = observer(({ show }) => {
  const isSelected = (gender) => {
    if (newPlayer.gender === gender) { return 'genderActiveButton'; }
    return 'genderButton';
  };

  return (
    <div className={styles.modalPosition}>
      <div className={styles.modal}>
        <div className={styles.close}>
          <Button
            className={styles.closeButton}
            image="/static/images/close.svg"
            click={show}
          />
        </div>
        <H1 text="Добавьте игрока" />
        <div className={styles.nameInput}>
          <div className={styles.name}>
            <div className={styles.text}>ФИО</div>
            <Input
              placeholder="Иванов Иван Иванович"
              className={styles.textInput}
              change={(event) => newPlayer.name = event.target.value}
            />
          </div>
        </div>
        <div className={styles.ageGenderBox}>
          <div className={styles.age}>
            <div className={styles.text}>Возраст</div>
            <Input
              placeholder="0"
              className={styles.ageInput}
              change={(event) => newPlayer.age = event.target.value}
            />
          </div>
          <div className={styles.gender}>
            <div className={styles.text}>Пол</div>
            <div className={styles.genderIcons}>
              <Button
                className={`${styles.genderButton} ${styles[isSelected('women')]}`}
                image="/static/images/women.svg"
                click={() => changeGender('women')}
              />
              <Button
                className={`${styles.genderButton} ${styles[isSelected('men')]}`}
                image="/static/images/men.svg"
                click={() => changeGender('men')}
              />
            </div>
          </div>
        </div>
        <Button
          text="Добавить"
          className={styles.add}
          click={() => {
            addPlayer({ ...newPlayer });
            show();
          }}
        />
      </div>
    </div>
  );
});

export default AddPlayer;
