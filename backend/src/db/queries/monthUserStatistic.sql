SELECT users.id as users_id, 
	users."surName" as surname,
	users."firstName" as firstName,
	users."secondName" as secondName,
	date_trunc('month', games."gameBegin") AS period,
	COUNT(games.id) FILTER (WHERE games."idPlayer1"= users.id AND "gameResult" = true) AS x_wins_cnt,
	COUNT(games.id) FILTER (WHERE games."idPlayer1"= users.id AND "gameResult" = false) AS x_loses_cnt,
	COUNT(games.id) FILTER (WHERE "gameResult" IS null) AS draws_cnt,
	COUNT(games.id) FILTER (WHERE games."idPlayer2"= users.id AND "gameResult" = true) AS o_wins_cnt,
	COUNT(games.id) FILTER (WHERE games."idPlayer2"= users.id AND "gameResult" = false) AS o_loses_cnt
FROM users 
	JOIN games ON users.id = games."idPlayer1" OR users.id = games."idPlayer2"
GROUP BY users.id, date_trunc('month', games."gameBegin")