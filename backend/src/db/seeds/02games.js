const { faker } = require('@faker-js/faker');
const gamesCount = 2 * Math.pow(10, 3);
const users = 4000;

exports.seed = function(knex) {
  const games = [];

  for (let i = 0; i < gamesCount; i++) {

    const startDate = new Date("2022-05-01");
    const endDate = new Date("2023-05-01");
    let beginDate;
    const player1 = faker.number.int({ min: 1, max: users });
    let player2;
    const gameResult = faker.number.int(2);


    do
      player2 = faker.number.int({ min: 1, max: users });
    while (player2 === player1);

    if (player1<users/10 || player2<users/10)
      beginDate = haveOneHighlightMonth(player1, player2);
    else if (player1<users/10*2 || player2<users/10*2)
      beginDate = haveGamesOnlyInMayNovemver(player1, player2);
    else if (player1<users/10*3 || player2<users/10*3)
      beginDate = haveSkippedMonth(player1, player2)
    else
      beginDate=faker.date.between({ from: startDate, to: endDate });

    const game = {
      id : i,
      idPlayer1: player1,
      idPlayer2: player2,
      gameResult: gameResult===0 ? false : gameResult===1 ? true : null,
      gameBegin: beginDate,
      gameEnd : null,
    };
    games.push(game);
  }
  const batch = knex.batchInsert("games", games);

  return Promise.all([batch]);
};


function haveGamesOnlyInMayNovemver() {
  let startDate = new Date("2022-05-01");
  let endDate = new Date("2022-11-01");
  return faker.date.between({ from: startDate, to: endDate });
}

function haveOneHighlightMonth(id1,id2) {
  let startDate = new Date("2022-12-01");
  let endDate = new Date("2022-12-30");

  if (id1<users/10) {
    startDate.setMonth(startDate.getMonth() + (id1 % 6));
    endDate.setMonth(endDate.getMonth() + (id1 % 6));
  }

  else {
    startDate.setMonth(startDate.getMonth() + (id2 % 6));
    endDate.setMonth(endDate.getMonth() + (id2 % 6));
  }

  return faker.date.between({ from: startDate, to: endDate });
}

function haveSkippedMonth(id1,id2) {
  let startDate = new Date("2022-12-01");
  let endDate = new Date("2023-05-01");
  let skippedMonth;
  let result;

  if (id1<users/10*3)
    skippedMonth = id1%6;
  else
    skippedMonth =id2%6;

  do
    result = faker.date.between({ from: startDate, to: endDate });
  while 
    (result.getMonth() === skippedMonth)
  
  return result;
}