const { faker } = require('@faker-js/faker');
const bcrypt = require('bcrypt');


exports.seed = (knex) => {
  const accounts = [];

  for (let i = 1; i < 40; i++) {
    const login = 'user' + i;

    const account = {
      idUser: i,
      login: login ,
      password : bcrypt.hashSync(login,10),
    }

    accounts.push(account);
  }

  return knex('accounts').insert(accounts);
};
