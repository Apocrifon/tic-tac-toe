const { fakerRU } = require('@faker-js/faker');

exports.seed = (knex) => {
  const users = [];

  for (let i = 1; i <= 4000; i++) {
    const gender = fakerRU.person.sexType();
    const blocked = fakerRU.number.int({ min: 0, max: 10 });

    const user = {
      id: i,
      surName: fakerRU.person.lastName(gender),
      firstName: fakerRU.person.firstName(gender),
      secondName: fakerRU.person.middleName(gender),
      age: fakerRU.number.int({ min: 1, max: 90 }),
      gender: gender === 'male' ? 1 : 0,
      statusActive: blocked === 0 ? false : true,
      statusGame : blocked === 1 ? fakerRU.number.binary() : 0
    };
    users.push(user);
  }

  return knex('users').insert(users);

};
