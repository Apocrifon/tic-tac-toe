module.exports = {
  development: {
    client: 'postgresql',
    connection: {
      host : 'db',
      port : 5432,
      database: 'test',
      user:     'postgres',
      password: 'zxc'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      directory: './migrations',
      tableName: 'knex_migrations'
    },
    seeds: {
      directory: './seeds'
    },
    useNullAsDefault: true
  },
};
