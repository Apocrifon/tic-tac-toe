exports.up = function(knex) {
	return knex.raw(
		`CREATE MATERIALIZED VIEW rating AS
	WITH facts AS (
			SELECT users.id AS users_id,
				users."surName" as surname,
				users."firstName" as firstName,
				users."secondName" as secondName,
				date_trunc('month', games."gameBegin") AS period,
				COUNT(games.id) FILTER (WHERE games."idPlayer1"= users.id AND "gameResult" = true) AS x_wins_cnt,
				COUNT(games.id) FILTER (WHERE games."idPlayer1"= users.id AND "gameResult" = false) AS x_loses_cnt,
				COUNT(games.id) FILTER (WHERE "gameResult" IS null) AS draws_cnt,
				COUNT(games.id) FILTER (WHERE games."idPlayer2"= users.id AND "gameResult" = true) AS o_wins_cnt,
				COUNT(games.id) FILTER (WHERE games."idPlayer2"= users.id AND "gameResult" = false) AS o_loses_cnt
			FROM users
					JOIN games ON users.id = games."idPlayer1" OR users.id = games."idPlayer2"
			WHERE games."gameBegin" >= now() - interval '6 month' AND users."statusActive" = true
			GROUP BY users.id, date_trunc('month', games."gameBegin")
	)
	SELECT 
		users_id,
			surname,
			firstname,
			secondname,
			AVG(x_wins_cnt * 0.9 + o_wins_cnt - x_loses_cnt * 1.1 + o_wins_cnt + draws_cnt/4) AS rating,
			SUM(x_wins_cnt + o_wins_cnt + x_loses_cnt + o_wins_cnt + draws_cnt) AS games_cnt,
			SUM(x_wins_cnt+o_wins_cnt) as games_wins,
			SUM(x_loses_cnt+o_wins_cnt) as games_loses,
			SUM(x_wins_cnt + o_wins_cnt) / SUM(x_wins_cnt + o_wins_cnt + x_loses_cnt + o_loses_cnt + draws_cnt) AS percent
	FROM facts
	GROUP BY users_id,surname,firstname,secondname
	ORDER BY rating DESC, users_id`
	)
};


exports.down = function(knex) {
	return knex.raw(
		`DROP MATERIALIZED VIEW IF EXISTS rating`
	)
};
