exports.up = function(knex) {
	return knex.schema
		.createTable('messenger', table => {
			table.increments('id');
			table.integer('idGame').references('id').inTable('games');
			table.integer('idUser').references('id').inTable('users');
			table.text('message').notNullable();
			table.timestamp('createdAt', { useTz: false }).notNullable();
		})
		
};

exports.down = function(knex) {
	return knex.schema
		.dropTable('messenger');
};
