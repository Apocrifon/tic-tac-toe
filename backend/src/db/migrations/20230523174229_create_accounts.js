exports.up = function(knex) {
	return knex.schema
		.createTable('accounts', table => {
			table.integer('idUser').references('id').inTable('users');
			table.string('login', 32).notNullable();
			table.string('password', 128).notNullable();
		})	
};

exports.down = function(knex) {
	return knex.schema
		.dropTable('accounts');
};
