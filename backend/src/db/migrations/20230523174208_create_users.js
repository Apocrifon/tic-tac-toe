exports.up = function(knex) {
	return knex.schema
		.createTable('users', table => {

			table.increments('id').defaultTo(1);
			table.string('surName').notNullable();
			table.string('firstName').notNullable();
			table.string('secondName').notNullable();
			table.integer('age').notNullable();
			table.boolean('gender').notNullable();
			table.boolean('statusActive').notNullable();
			table.boolean('statusGame').notNullable();
			table.timestamp('createdAt', { useTz: false }).defaultTo(knex.fn.now());
			table.timestamp('updatedAt', { useTz: false }).defaultTo(knex.fn.now());
		})
};

exports.down = function (knex) {
	
  return knex.schema
		.dropTable('users');
};
