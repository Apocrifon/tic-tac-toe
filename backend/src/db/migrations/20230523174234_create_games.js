exports.up = function(knex) {
	return knex.schema
		.createTable('games', table => {
			table.increments('id');
			table.integer('idPlayer1').references('id').inTable('users');
			table.integer('idPlayer2').references('id').inTable('users');
			table.boolean('gameResult');
			table.timestamp('gameBegin', { useTz: false }).notNullable();
			table.timestamp('gameEnd', { useTz: false }).defaultTo(null);
		})	
};

exports.down = function (knex) {
	
	return knex.schema
		.dropTable('games');
};
