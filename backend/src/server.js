require('dotenv').config();
const express = require('express');
const app = express();
const http = require('http');
const { Server } = require('socket.io');
const server = http.createServer(app);
const cors = require('cors');
const bodyParser = require('body-parser')
const checkJWT = require('./api/middlewares/checkJWT');
const auth = require('./api/routes/auth');
const api = require('./api/routes/api');
const field = require('./api/routes/field');
const rating = require('./api/routes/rating');
const io = new Server(server, {
	cors: {
		origin: 'http://frontend:3000',
		methods: ['GET', 'POST'],
		allowedHeaders: ["Authorization"],
	},
});
global.fields = [];

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cors());

app.use('/auth', auth);

app.use(checkJWT); 
app.use('/api', api);
app.use('/fields', field);
app.use('/rating', rating);

io.on('connection', (socket) => {
	console.log(`user connected ${socket.id}`);

	socket.on('sendField', cell => {
		socket.broadcast.emit('recieveField', cell);
	}) 

	socket.on('sendMessage', messages => {
		io.emit('recieveMessage', messages);
	})  

	socket.on('sendInvite', invite => {
		io.emit('recieveInvite', invite);
	})  

})

server.listen(process.env.DEV_PORT, () => console.log(`server run on port ${process.env.DEV_PORT}`));