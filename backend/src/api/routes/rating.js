const express = require('express')
const router = express.Router()
const knexConfig = require('../../db/knexfile');
const knex = require('knex')(knexConfig.development)



router.get('/', (req, res) => {
  try {
    knex
      .select(
        knex.raw('ROW_NUMBER() OVER (ORDER BY rating DESC) AS position'),
        'surname',
        'firstname',
        'secondname',
        'games_cnt',
        'games_wins',
        'games_loses',
        'percent')
      .from('rating')
      .then((rows) => {
        res.json(rows);
      })
  }
  catch(error) {
      res.status(501).json({ error: 'Internal Server Error' });
    };
})

router.delete('/', (req, res) => {
	knex('games').del()
	.then(()=> console.log('OK'))
	res.send();
})


module.exports = router;