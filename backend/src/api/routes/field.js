const express = require('express')
const router = express.Router()




router.get('/', (req, res) => {
	if (req.query.index < 0 || req.query.index > fields.length - 1)
		res.status(404).json({ error: 'Internal Server Error' });
	res.send(JSON.stringify(fields.at(req.query.index)));
})

router.post('/', (req, res) => {
	if (!req.body.field)
		res.status(400).json({ error: 'Internal Server Error' });
	
	fields.push(req.body.field);
	res.status(200).send();
})

router.put('/', (req, res) => {
	if (req.body.index < 0 || req.body.index > fields.length - 1)
		res.status(404).json({ error: 'Internal Server Error' });
	fields[req.body.index] = req.body.field;
	res.send();
})

router.delete('/', (req, res) => {
	if (req.query.index < 0 || req.query.index > fields.length - 1)
		res.status(404).json({ error: 'Internal Server Error' });
	fields.splice(req.query.index, 1);
	res.send();
})

module.exports = router;
