const express = require('express');
const router = express.Router();

router.post('/token', (req, res) => {
	res.json({ isAuth: true });
});


module.exports = router
