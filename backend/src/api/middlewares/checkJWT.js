const jwt = require('jsonwebtoken');
require('dotenv').config();

checkJWT = (req, res, next) => {
	const token = req.headers.authorization.split(' ')[1];

	if (!token)
		return res.sendStatus(401);
	
	try {
		const decoded = jwt.verify(token, process.env.TOKEN_SECRET);
		req.body.user = decoded;
		next();
	} catch (error) {
		return res.sendStatus(403);
	}
}

module.exports = checkJWT;