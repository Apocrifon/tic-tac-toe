const knexConfig = require('../../db/knexfile');
require('dotenv').config();
const knex = require('knex')(knexConfig.development)
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const auth = async (req, res) => {
	const login = req.body.login;
	const password = req.body.password;

	if (!login || !password)
		return res.sendStatus(403);

	checkPassword(login, password).then(result => {
		if (result.status === 'user_not_found')
			res.sendStatus(404);
		else if (result.status === 'password_not_matched') {
			res.sendStatus(401)
		}
		else
      res.json({token : jwt.sign({ id: result.id }, process.env.TOKEN_SECRET), user: result.userId });
		})
}

async function checkPassword(login, password) {
  const result = {
    status: '',
    userId: null
  };

  const user = await knex('accounts')
    .select('idUser', 'login', 'password')
    .where('login', '=', login)
    .first();

  if (!user) {
    result.status = 'user_not_found';
  } else if (bcrypt.compareSync(password, user.password)) {
    result.status = 'password_matched';
    result.userId = user.idUser;
  } else {
    result.status = 'password_not_matched';
  }

  return result;
}

module.exports = auth;