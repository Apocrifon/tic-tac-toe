const jwt = require('jsonwebtoken');
const TOKEN_SECRET = 'e7b0f4a0ce2c60f037ef3d09462db8b449607b4e682b8b73982d5'+
	'0fb3c3b0457f7b31db625f1a097c017159c014f70f7854ad0eb0c3e7d6e835ed038abbd57f'; // replace to env 

async function token(req, res) {
		const authHeader = req.headers.authorization;
		if (authHeader) {
			const token = authHeader.split(' ')[1];
			jwt.verify(token, TOKEN_SECRET, (error, decode) => {
				if (error) {
					return res.json({isAuth: false})
				}
				res.json({ isAuth: true }); 
			})
		} else {
			res.sendStatus(401);
		}
};

module.exports = token;